INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (1, 'Category',  'HI-FI', 1);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (2, 'Technology',  'LCD', 1);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (3, 'Category',  'Low End', 2);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (4, 'Technology',  'VA', 2);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (5, 'Category',  'HI-END', 3);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (6, 'Technology',  'VA', 3);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (7, 'Category',  'HI-FI', 4);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (8, 'Technology',  'LCD', 4);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (9, 'Category',  'Low End', 5);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (10, 'Technology',  'VA', 5);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (11, 'Category',  'HI-END', 6);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (12, 'Technology',  'VA', 6);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (13, 'Dust value',  '12', 7);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (14, 'mode',  '9', 7);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (15, 'Dust value',  '1', 8);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (16, 'mode',  '6', 8);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (17, 'Dust value',  '2', 9);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (18, 'mode',  '6', 9);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (19, 'Dust value',  '7', 10);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (20, 'mode',  '1', 10);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (21, 'Dust value',  '10', 11);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (22, 'mode',  '3', 11);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (23, 'Dust value',  '15',12);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (24, 'mode',  '5', 12);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (25, 'doors',  '1', 13);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (26, 'compressor',  '5', 13);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (27, 'doors',  '2', 14);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (28, 'compressor',  '6', 14);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (29, 'doors',  '3', 15);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (30, 'compressor',  '4', 15);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (31, 'doors',  '4', 16);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (32, 'compressor',  '3', 16);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (33, 'doors',  '1', 17);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (34, 'compressor',  '2', 17);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (35, 'doors',  '3', 18);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (36, 'compressor',  '2', 18);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (37, 'RAM',  '2', 19);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (38, 'cameras',  '2', 19);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (39, 'RAM',  '1', 20);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (40, 'cameras',  '3', 20);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (41, 'RAM',  '3', 21);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (42, 'cameras',  '1', 21);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (43, 'RAM',  '6', 22);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (44, 'cameras',  '2', 22);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (45, 'RAM',  '7', 23);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (46, 'cameras',  '5', 23);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (47, 'RAM',  '5', 24);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (48, 'cameras',  '3', 24);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (49, 'type',  '3', 25);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (50, 'CPU',  '300', 25);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (51, 'type',  '4', 26);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (52, 'CPU',  '400', 26);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (53, 'type',  '5', 27);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (54, 'CPU',  '500', 27);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (55, 'type',  '31', 28);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (56, 'CPU',  '311', 28);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (57, 'type',  '54', 29);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (58, 'CPU',  '543', 29);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (59, 'type',  '7664', 30);
INSERT INTO product_property(id, name_property, value_property, product_id) VALUES (60, 'CPU',  '2123434', 30);
