create table type
(
    id                bigserial
        primary key,
    title             varchar(255),
    company_producer   varchar(255),
    country_producer   varchar(255),
    is_available_on_line boolean,
    is_installment     boolean
);

create table product
(
    id           bigserial
        primary key,
    product_name  varchar(255),
    serial_number varchar(255),
    color        varchar(255),
    size         numeric(19, 2),
    price        numeric(19, 2),
    in_stock      boolean,
    type_id      bigint
        constraint fktpl21yaxf90l0f2blv2mialn3
            references type
);

create table productproperty
(
    id            bigserial
        primary key,
    name_property  varchar(255),
    value_property varchar(255),
    product_id    bigint
        constraint fk7f0h4xxw47iamxyjhgs55l1uj
            references product
);
