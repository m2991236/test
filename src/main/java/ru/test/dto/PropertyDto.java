package ru.test.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.test.models.ProductProperty;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class PropertyDto {
    private Long id;
    private String nameProperty;
    private String valueProperty;

    public static PropertyDto from(ProductProperty productProperty) {
        return PropertyDto.builder()
                .id(productProperty.getId())
                .nameProperty(productProperty.getNameProperty())
                .valueProperty(productProperty.getValueProperty())
                .build();
    }

    public static List<PropertyDto> from(List<ProductProperty> productProperties) {
        return productProperties.stream().map(PropertyDto::from).collect(Collectors.toList());
    }
}

