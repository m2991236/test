package ru.test.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.test.models.Product;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ProductDto {
    private Long id;
    private String productName;
    private String serialNumber;
    private String color;
    private BigDecimal size;
    private BigDecimal price;
    private Boolean inStock;


    public static ProductDto from(Product product) {
        return ProductDto.builder()
                .id(product.getId())
                .productName(product.getProductName())
                .serialNumber(product.getSerialNumber())
                .color(product.getColor())
                .size(product.getSize())
                .price(product.getPrice())
                .inStock(product.getInStock())
                .build();
    }

    public static List<ProductDto> from(List<Product> merches) {
        return merches.stream().map(ProductDto::from).collect(Collectors.toList());
    }
}
