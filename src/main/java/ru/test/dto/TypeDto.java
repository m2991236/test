package ru.test.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.test.models.Type;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class TypeDto {
    private Long id;
    private String title;
    private String countryProducer;
    private String companyProducer;
    private Boolean isAvailableOnLine;
    private Boolean isInstallment;

    public static TypeDto from(Type type) {
        return TypeDto.builder()
                .id(type.getId())
                .title(type.getTitle())
                .countryProducer(type.getCountryProducer())
                .companyProducer(type.getCompanyProducer())
                .isAvailableOnLine(type.getIsAvailableOnLine())
                .isInstallment(type.getIsInstallment())
                .build();
    }

    public static List<TypeDto> from(List<Type> types) {
        return types.stream().map(TypeDto::from).collect(Collectors.toList());
    }
}
