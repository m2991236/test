package ru.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.test.models.Type;

public interface TypesRepository extends JpaRepository<Type, Long> {
}
