package ru.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.test.models.Product;
import ru.test.models.ProductProperty;

import java.util.List;

public interface PropertiesRepository extends JpaRepository<ProductProperty, Long> {
    List<ProductProperty> findAllByProductIs(Product id);
}
