package ru.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.test.dto.ProductDto;
import ru.test.models.Product;

import java.math.BigDecimal;
import java.util.List;

public interface ProductsRepository extends JpaRepository<Product, Long> {
    List<ProductDto> findAllByProductNameLikeIgnoreCase(String productName);

    List<ProductDto> findAllByType_Id(Long id);

    List<ProductDto> findAllByColorLikeIgnoreCase(String color);

    List<ProductDto> findAllByPriceBetween(BigDecimal minPrice, BigDecimal maxPrice);

}
