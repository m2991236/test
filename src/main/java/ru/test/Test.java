package ru.test;

import org.springframework.boot.SpringApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "test", version = "1.0", description = "test"))
public class Test {

    public static void main(String[] args) {
        SpringApplication.run(Test.class, args);
    }

}
