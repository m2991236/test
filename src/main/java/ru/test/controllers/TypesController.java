package ru.test.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.test.dto.ProductsResponse;
import ru.test.dto.TypeDto;
import ru.test.services.TypesService;

import java.math.BigDecimal;

@Tag(name = "Type card", description = "The Type API")
@RestController
@RequestMapping("/api")
public class TypesController {

    @Autowired
    private TypesService typesService;

    @Operation(summary = "Add type card", tags = "Type card")
    @PostMapping("/type")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<TypeDto> addType(@RequestBody TypeDto typeDto) {
        return ResponseEntity.ok(typesService.addType(typeDto));
    }

    @Operation(summary = "Get type card", tags = "Type card")
    @GetMapping("/type/{type-id}")
    public ResponseEntity<TypeDto> getType(@PathVariable("type-id") Long id) {
        return ResponseEntity.ok(typesService.getType(id));
    }

    @Operation(summary = "Get type card by productName", tags = "Type card")
    @GetMapping("/type/{type-id}/name")
    public ResponseEntity<ProductsResponse> getTypeByProductName(@RequestParam("type-id") Long id,
                                                                 @RequestParam("product") String productName) {
        return ResponseEntity.ok().body(ProductsResponse.builder().data(typesService
                .getTypeByName(id, productName)).build());
    }

    @Operation(summary = "Get type card by serial number", tags = "Type card")
    @GetMapping("/type/{type-id}/serialNumber")
    public ResponseEntity<ProductsResponse> getTypeBySerialNumber(@RequestParam("type-id") Long id,
                                                                  @RequestParam("serial") String serialNumber) {
        return ResponseEntity.ok().body(ProductsResponse.builder().data(typesService
                .getTypeBySerialNumber(id, serialNumber)).build());
    }

    @Operation(summary = "Get type card by color", tags = "Type card")
    @GetMapping("/type/{type-id}/color")
    public ResponseEntity<ProductsResponse> getTypeByColor(@RequestParam("type-id") Long id,
                                                           @RequestParam("color") String color) {
        return ResponseEntity.ok().body(ProductsResponse.builder().data(typesService
                .getTypeByColor(id, color)).build());
    }

    @Operation(summary = "Get type card by size", tags = "Type card")
    @GetMapping("/type/{type-id}/size")
    public ResponseEntity<ProductsResponse> getTypeBySize(@RequestParam("type-id") Long id,
                                                          @RequestParam("size") BigDecimal size) {
        return ResponseEntity.ok().body(ProductsResponse.builder().data(typesService
                .getTypeBySize(id, size)).build());
    }

    @Operation(summary = "Get type card by present", tags = "Type card")
    @GetMapping("/type/{type-id}/present")
    public ResponseEntity<ProductsResponse> getTypeByPresent(@RequestParam("type-id") Long id,
                                                             @RequestParam("present") Boolean present) {
        return ResponseEntity.ok().body(ProductsResponse.builder().data(typesService
                .getTypeByPresent(id, present)).build());
    }

    @Operation(summary = "Get type card by additional property", tags = "Type card")
    @GetMapping("/type/{type-id}/property")
    public ResponseEntity<ProductsResponse> getTypeByProperty(@RequestParam("type-id") Long id,
                                                              @RequestParam("propertyName") String propertyName,
                                                              @RequestParam("propertyValue") String propertyValue) {
        return ResponseEntity.ok().body(ProductsResponse.builder().data(typesService
                .getTypeByProperty(id, propertyName, propertyValue)).build());
    }

}
