package ru.test.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.test.dto.ProductDto;
import ru.test.dto.PropertyDto;
import ru.test.dto.ProductsResponse;
import ru.test.dto.PropertyResponse;
import ru.test.services.ProductsService;

import java.math.BigDecimal;

@Tag(name = "Product card", description = "The Product API")
@RestController
@RequestMapping("/api")
public class ProductsController {

    @Autowired
    private ProductsService productsService;

    @Operation(summary = "Get product card", tags = "Product card")
    @GetMapping("/prods/{prod-id}")
    public ResponseEntity<ProductDto> getProduct(@PathVariable("prod-id") Long id) {
        return ResponseEntity.ok(productsService.getProduct(id));
    }


    @Operation(summary = "Gets product cards sorted by name", tags = "Products card")
    @GetMapping("/prodsSortedByName")
    public ResponseEntity<ProductsResponse> getProductsSortedByName(@RequestParam("page") int page,
                                                                    @RequestParam("size") int size) {

        return ResponseEntity.ok()
                .body(ProductsResponse.builder().data(productsService.getProductsSortByName(page, size)).build());
    }

    @Operation(summary = "Gets product cards sorted by price", tags = "Products card")
    @GetMapping("/prodsSortedByPrice")
    public ResponseEntity<ProductsResponse> getProducts(@RequestParam("page") int page,
                                                         @RequestParam("size") int size) {

        return ResponseEntity.ok()
                .body(ProductsResponse.builder().data(productsService.getProductsSortByPrice(page, size)).build());
    }

    @Operation(summary = "Gets Product cards by name", tags = "Product card")
    @GetMapping("/products/")
    public ResponseEntity<ProductsResponse> getProductByName(@RequestParam("productName") String productName) {
        return ResponseEntity.ok()
                .body(ProductsResponse.builder().data(productsService
                        .searchByName(productName)).build());
    }


    @Operation(summary = "Gets products cards by Type", tags = "Products card")
    @GetMapping("/productsByType")
    public ResponseEntity<ProductsResponse> getProductByType(@RequestParam("type") Long id) {

        return ResponseEntity.ok()
                .body(ProductsResponse.builder().data(productsService.
                        searchByType(id)).build());
    }

    @Operation(summary = "Gets products cards by color", tags = "Products card")
    @GetMapping("/productsByColor")
    public ResponseEntity<ProductsResponse> getProductByColor(@RequestParam("color") String color) {

        return ResponseEntity.ok()
                .body(ProductsResponse.builder().data(productsService.
                        searchByColor(color)).build());
    }

    @Operation(summary = "Gets product cards by price", tags = "Product card")
    @GetMapping("/productsByPrice")
    public ResponseEntity<ProductsResponse> getProductByPrice(@RequestParam("minPrice") BigDecimal minPrice,
                                                              @RequestParam("maxPrice") BigDecimal maxPrice) {

        return ResponseEntity.ok()
                .body(ProductsResponse.builder().data(productsService
                                .getProductByPrice(minPrice, maxPrice))
                        .build());
    }

    @Operation(summary = "Add product cards", tags = "Product card")
    @PostMapping("/prods")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ProductDto> addProduct(@RequestParam("Type-id") Long typeId,
                                                 @RequestBody ProductDto productDto) {
        return ResponseEntity.ok(productsService.addProductToType(typeId, productDto));
    }

    @Operation(summary = "Add property to the product cards", tags = "Product card")
    @PostMapping(value = "/{product-id}/property")
    @ResponseStatus(HttpStatus.CREATED)
    public PropertyResponse addPropertyToProduct(@PathVariable("product-id") Long productId,
                                                 @RequestBody PropertyDto propertyDto) {
        return PropertyResponse.builder()
                .data(productsService.addPropertyToProduct(productId, propertyDto))
                .build();
    }

}
