package ru.test.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.test.dto.PropertyDto;
import ru.test.dto.PropertyResponse;
import ru.test.services.PropertiesService;

@Tag(name = "Property card", description = "The Property API")
@RestController
@RequestMapping("/api")
public class PropertiesController {

    @Autowired
    private PropertiesService propertiesService;

    @Operation(summary = "Add property card", tags = "Property card")
    @PostMapping("/property")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<PropertyDto> addProperty(@RequestBody PropertyDto propertyDto) {
        return ResponseEntity.ok(propertiesService.addProperty(propertyDto));
    }

    @Operation(summary = "Get property card", tags = "Property card")
    @GetMapping("/property/{property-id}")
    public ResponseEntity<PropertyDto> getProperty(@PathVariable("property-id") Long id) {
        return ResponseEntity.ok(propertiesService.getProperty(id));
    }


    @Operation(summary = "Get product properties", tags = "Products card")
    @GetMapping("/properties")
    public ResponseEntity<PropertyResponse> getProductProperties(@RequestParam("productId") Long productId) {

        return ResponseEntity.ok()
                .body(PropertyResponse.builder().data(propertiesService
                        .getPropertyByProductId(productId)).build());
    }

}
