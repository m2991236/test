package ru.test.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
public class Type {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    @Column(name = "country_producer")
    private String countryProducer;
    @Column(name = "company_producer")
    private String companyProducer;
    @Column(name = "is_available_on_line")
    private Boolean isAvailableOnLine;
    @Column(name = "is_installment")
    private Boolean isInstallment;

    @OneToMany(mappedBy = "type")
    private List<Product> products;

}
