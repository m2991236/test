package ru.test.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "product_property")
public class ProductProperty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name_property")
    private String nameProperty;
    @Column(name = "value_property")
    private String valueProperty;

    @ManyToOne
    private Product product;
}
