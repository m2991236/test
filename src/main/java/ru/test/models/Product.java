package ru.test.models;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "product_name")
    private String productName;
    @Column(name = "serial_number")
    private String serialNumber;
    private String color;
    private BigDecimal size;
    private BigDecimal price;
    @Column(name = "in_stock")
    private Boolean inStock;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private Type type;

    @OneToMany
    @JoinColumn(name = "product_id")
    private List<ProductProperty> productProperties;

}
