package ru.test.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.test.dto.ProductDto;
import ru.test.dto.TypeDto;
import ru.test.models.Product;
import ru.test.models.ProductProperty;
import ru.test.models.Type;
import ru.test.repositories.TypesRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static ru.test.dto.TypeDto.from;
import static ru.test.dto.ProductDto.from;

@Service
@RequiredArgsConstructor
public class TypesServiceImpl implements TypesService {

    private final TypesRepository typesRepository;

    public TypeDto addType(TypeDto type) {
        Type newType = Type.builder()
                .title(type.getTitle())
                .countryProducer(type.getCountryProducer())
                .companyProducer(type.getCompanyProducer())
                .isAvailableOnLine(type.getIsAvailableOnLine())
                .isInstallment(type.getIsInstallment())
                .build();
        typesRepository.save(newType);
        return from(newType);
    }

    @Override
    public TypeDto getType(Long id) {
        return from(typesRepository.getById(id));
    }

    @Override
    public List<ProductDto> getTypeByName(Long id, String productName) {
        List<ProductDto> productDtos = from(typesRepository.getById(id).getProducts());
        List<ProductDto> res = new ArrayList<>();
        for (ProductDto p : productDtos) {
            if (p.getProductName().equalsIgnoreCase(productName)) {
                res.add(p);
            }
        }
        return res;
    }

    @Override
    public List<ProductDto> getTypeBySerialNumber(Long id, String serialNumber) {
        List<ProductDto> productDtos = from(typesRepository.getById(id).getProducts());
        List<ProductDto> res = new ArrayList<>();
        for (ProductDto p : productDtos) {
            if (p.getSerialNumber().equalsIgnoreCase(serialNumber)) {
                res.add(p);
            }
        }
        return res;
    }

    @Override
    public List<ProductDto> getTypeByColor(Long id, String color) {
        List<ProductDto> productDtos = from(typesRepository.getById(id).getProducts());
        List<ProductDto> res = new ArrayList<>();
        for (ProductDto p : productDtos) {
            if (p.getColor().equalsIgnoreCase(color)) {
                res.add(p);
            }
        }
        return res;
    }

    @Override
    public List<ProductDto> getTypeBySize(Long id, BigDecimal size) {
        List<ProductDto> productDtos = from(typesRepository.getById(id).getProducts());
        List<ProductDto> res = new ArrayList<>();
        for (ProductDto p : productDtos) {
            if (p.getSize().compareTo(size) == 0) {
                res.add(p);
            }
        }
        return res;
    }

    @Override
    public List<ProductDto> getTypeByPresent(Long id, Boolean present) {
        List<ProductDto> productDtos = from(typesRepository.getById(id).getProducts());
        List<ProductDto> res = new ArrayList<>();
        for (ProductDto p : productDtos) {
            if (p.getInStock() == present) {
                res.add(p);
            }
        }
        return res;
    }

    @Override
    public List<ProductDto> getTypeByProperty(Long id, String propertyName, String propertyValue) {
        List<Product> product = typesRepository.getById(id).getProducts();
        List<Product> res = new ArrayList<>();
        List<ProductProperty> productProperties;
        for (Product p : product) {
            productProperties = p.getProductProperties();
            for (ProductProperty prop : productProperties) {
                if (prop.getNameProperty().equalsIgnoreCase(propertyName) && prop.getValueProperty().equalsIgnoreCase(propertyValue)) {
                    res.add(p);
                }
            }
        }
        return from(res);
    }
}
