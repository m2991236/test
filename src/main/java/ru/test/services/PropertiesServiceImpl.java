package ru.test.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.test.dto.PropertyDto;
import ru.test.models.Product;
import ru.test.models.ProductProperty;
import ru.test.repositories.ProductsRepository;
import ru.test.repositories.PropertiesRepository;

import java.util.List;

import static ru.test.dto.PropertyDto.from;

@Service
@RequiredArgsConstructor
public class PropertiesServiceImpl implements PropertiesService {

    private final PropertiesRepository propertiesRepository;
    private final ProductsRepository productsRepository;

    @Override
    public PropertyDto getProperty(Long id) {
        return from(propertiesRepository.getById(id));
    }

    @Override
    public List<PropertyDto> getPropertyByProductId(Long id) {
        Product existProduct = productsRepository.getById(id);
        return from(propertiesRepository.findAllByProductIs(existProduct));
    }

    @Override
    public PropertyDto addProperty(PropertyDto propertyDto) {
        ProductProperty newProperty = ProductProperty.builder()
                .nameProperty(propertyDto.getNameProperty())
                .valueProperty(propertyDto.getValueProperty())
                .build();
        propertiesRepository.save(newProperty);
        return from(newProperty);
    }
}
