package ru.test.services;

import ru.test.dto.PropertyDto;

import java.util.List;

public interface PropertiesService {
    PropertyDto getProperty(Long id);

    List<PropertyDto> getPropertyByProductId(Long id);

    PropertyDto addProperty(PropertyDto propertyDto);

}
