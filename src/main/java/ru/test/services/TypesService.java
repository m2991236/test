package ru.test.services;

import ru.test.dto.ProductDto;
import ru.test.dto.TypeDto;

import java.math.BigDecimal;
import java.util.List;

public interface TypesService {
    TypeDto addType(TypeDto typeDto);

    TypeDto getType(Long id);

    List<ProductDto> getTypeByName(Long id, String productName);

    List<ProductDto> getTypeBySerialNumber(Long id, String serialNumber);

    List<ProductDto> getTypeByColor(Long id, String color);

    List<ProductDto> getTypeBySize(Long id, BigDecimal size);

    List<ProductDto> getTypeByPresent(Long id, Boolean present);

    List<ProductDto> getTypeByProperty(Long id, String propertyName, String propertyValue);
}
