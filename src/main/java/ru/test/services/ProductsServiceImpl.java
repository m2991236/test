package ru.test.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.test.dto.ProductDto;
import ru.test.dto.PropertyDto;
import ru.test.models.Product;
import ru.test.models.ProductProperty;
import ru.test.models.Type;
import ru.test.repositories.PropertiesRepository;
import ru.test.repositories.ProductsRepository;
import ru.test.repositories.TypesRepository;

import java.math.BigDecimal;
import java.util.List;

import static ru.test.dto.ProductDto.from;

@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductsService {

    private final TypesRepository typesRepository;
    private final ProductsRepository productsRepository;
    private final PropertiesRepository propertiesRepository;

    @Override
    public ProductDto addProductToType(Long typeId, ProductDto product) {
        Type existedType = typesRepository.getById(typeId);
        Product newProduct = Product.builder()
                .productName(product.getProductName())
                .serialNumber(product.getSerialNumber())
                .color(product.getColor())
                .size(product.getSize())
                .price(product.getPrice())
                .inStock(product.getInStock())
                .type(existedType)
                .build();
        productsRepository.save(newProduct);
        return from(newProduct);
    }

    @Override
    public List<PropertyDto> addPropertyToProduct(Long productId, PropertyDto propertyDto) {
        Product product = productsRepository.getById(productId);
        ProductProperty existedProperty = propertiesRepository.getById(propertyDto.getId());
        product.getProductProperties().add(existedProperty);
        productsRepository.save(product);
        return PropertyDto.from(product.getProductProperties());
    }

    @Override
    public List<ProductDto> searchByName(String productName) {
        return productsRepository.findAllByProductNameLikeIgnoreCase("%" + productName + "%");
    }

    @Override
    public List<ProductDto> searchByType(Long id) {
        return productsRepository.findAllByType_Id(id);
    }

    @Override
    public List<ProductDto> searchByColor(String color) {
        return productsRepository.findAllByColorLikeIgnoreCase("%" + color + "%");
    }

    @Override
    public List<ProductDto> getProductByPrice(BigDecimal minPrice, BigDecimal maxPrice) {
        return productsRepository.findAllByPriceBetween(minPrice, maxPrice);
    }

    @Override
    public ProductDto getProduct(Long id) {
        return from(productsRepository.getById(id));
    }

    @Override
    public List<ProductDto> getProductsSortByName(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("productName"));
        return from(productsRepository.findAll(request).getContent());
    }

    @Override
    public List<ProductDto> getProductsSortByPrice(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("price"));
        return from(productsRepository.findAll(request).getContent());
    }

}
