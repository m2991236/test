package ru.test.services;

import ru.test.dto.ProductDto;
import ru.test.dto.PropertyDto;

import java.math.BigDecimal;
import java.util.List;

public interface ProductsService {
    ProductDto getProduct(Long id);

    List<ProductDto> getProductsSortByName(int page, int size);

    List<ProductDto> getProductsSortByPrice(int page, int size);

    ProductDto addProductToType(Long typeId, ProductDto product);

    List<PropertyDto> addPropertyToProduct(Long productId, PropertyDto propertyDto);

    List<ProductDto> searchByName(String productName);

    List<ProductDto> searchByType(Long id);

    List<ProductDto> searchByColor(String color);

    List<ProductDto> getProductByPrice(BigDecimal minPrice, BigDecimal maxPrice);
}
